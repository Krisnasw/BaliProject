package com.luwakdev.baliproject.model;

public class Nota {

    public Nota() {

    }

    private Integer nota_id;
    private String nota_code;
    private String nota_date;
    private Integer customer_id;
    private Integer employee_id;
    private Integer nota_type;
    private Integer coa_detail_id;
    private String nota_tempo;
    private String nota_credit_card;
    private String nota_desc;
    private Integer nota_status;
    private Integer nota_reference;
    private Integer warehouse_id;
    private String nota_member_card;
    private Integer nota_dp;
    private Integer nota_type_dp;
    private String nota_number_dp;
    private Integer nota_bill;
    private Integer nota_netto;
    private Integer nota_lock;
    private Integer nota_request;
    private Integer nota_status_delete;
    private Integer user_id;
    private String nota_faktur_tax;
    private String customer_address;
    private String customer_telp;

    public Nota(Integer nota_id, String nota_code, String nota_date, Integer customer_id,
                Integer employee_id, Integer nota_type, Integer coa_detail_id,
                String nota_tempo, String nota_credit_card, String nota_desc,
                Integer nota_status, Integer nota_reference, Integer warehouse_id,
                String nota_member_card, Integer nota_dp, Integer nota_type_dp,
                String nota_number_dp, Integer nota_bill, Integer nota_netto, Integer nota_lock, Integer nota_request,
                Integer nota_status_delete, Integer user_id, String nota_faktur_tax,
                String customer_address, String customer_telp) {

        this.nota_id = nota_id;
        this.nota_code = nota_code;
        this.nota_date = nota_date;
        this.customer_id = customer_id;
        this.employee_id = employee_id;
        this.nota_type = nota_type;
        this.coa_detail_id = coa_detail_id;
        this.nota_tempo = nota_tempo;
        this.nota_credit_card = nota_credit_card;
        this.nota_desc = nota_desc;
        this.nota_status = nota_status;
        this.nota_reference = nota_reference;
        this.warehouse_id = warehouse_id;
        this.nota_member_card = nota_member_card;
        this.nota_dp = nota_dp;
        this.nota_type_dp = nota_type_dp;
        this.nota_number_dp = nota_number_dp;
        this.nota_bill = nota_bill;
        this.nota_netto = nota_netto;
        this.nota_lock = nota_lock;
        this.nota_request = nota_request;
        this.nota_status_delete = nota_status_delete;
        this.user_id = user_id;
        this.nota_faktur_tax = nota_faktur_tax;
        this.customer_address = customer_address;
        this.customer_telp = customer_telp;

    }

    public Integer getNota_id() {
        return nota_id;
    }

    public void setNota_id(Integer nota_id) {
        this.nota_id = nota_id;
    }

    public String getNota_code() {
        return nota_code;
    }

    public void setNota_code(String nota_code) {
        this.nota_code = nota_code;
    }

    public String getNota_date() {
        return nota_date;
    }

    public void setNota_date(String nota_date) {
        this.nota_date = nota_date;
    }

    public Integer getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(Integer customer_id) {
        this.customer_id = customer_id;
    }

    public Integer getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(Integer employee_id) {
        this.employee_id = employee_id;
    }

    public Integer getNota_type() {
        return nota_type;
    }

    public void setNota_type(Integer nota_type) {
        this.nota_type = nota_type;
    }

    public Integer getCoa_detail_id() {
        return coa_detail_id;
    }

    public void setCoa_detail_id(Integer coa_detail_id) {
        this.coa_detail_id = coa_detail_id;
    }

    public String getNota_tempo() {
        return nota_tempo;
    }

    public void setNota_tempo(String nota_tempo) {
        this.nota_tempo = nota_tempo;
    }

    public String getNota_credit_card() {
        return nota_credit_card;
    }

    public void setNota_credit_card(String nota_credit_card) {
        this.nota_credit_card = nota_credit_card;
    }

    public String getNota_desc() {
        return nota_desc;
    }

    public void setNota_desc(String nota_desc) {
        this.nota_desc = nota_desc;
    }

    public Integer getNota_status() {
        return nota_status;
    }

    public void setNota_status(Integer nota_status) {
        this.nota_status = nota_status;
    }

    public Integer getNota_reference() {
        return nota_reference;
    }

    public void setNota_reference(Integer nota_reference) {
        this.nota_reference = nota_reference;
    }

    public Integer getWarehouse_id() {
        return warehouse_id;
    }

    public void setWarehouse_id(Integer warehouse_id) {
        this.warehouse_id = warehouse_id;
    }

    public String getNota_member_card() {
        return nota_member_card;
    }

    public void setNota_member_card(String nota_member_card) {
        this.nota_member_card = nota_member_card;
    }

    public Integer getNota_dp() {
        return nota_dp;
    }

    public void setNota_dp(Integer nota_dp) {
        this.nota_dp = nota_dp;
    }

    public Integer getNota_type_dp() {
        return nota_type_dp;
    }

    public void setNota_type_dp(Integer nota_type_dp) {
        this.nota_type_dp = nota_type_dp;
    }

    public String getNota_number_dp() {
        return nota_number_dp;
    }

    public void setNota_number_dp(String nota_number_dp) {
        this.nota_number_dp = nota_number_dp;
    }

    public Integer getNota_bill() {
        return nota_bill;
    }

    public void setNota_bill(Integer nota_bill) {
        this.nota_bill = nota_bill;
    }

    public Integer getNota_netto() {
        return nota_netto;
    }

    public void setNota_netto(Integer nota_netto) {
        this.nota_netto = nota_netto;
    }

    public Integer getNota_lock() {
        return nota_lock;
    }

    public void setNota_lock(Integer nota_lock) {
        this.nota_lock = nota_lock;
    }

    public Integer getNota_request() {
        return nota_request;
    }

    public void setNota_request(Integer nota_request) {
        this.nota_request = nota_request;
    }

    public Integer getNota_status_delete() {
        return nota_status_delete;
    }

    public void setNota_status_delete(Integer nota_status_delete) {
        this.nota_status_delete = nota_status_delete;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public String getNota_faktur_tax() {
        return nota_faktur_tax;
    }

    public void setNota_faktur_tax(String nota_faktur_tax) {
        this.nota_faktur_tax = nota_faktur_tax;
    }

    public String getCustomer_address() {
        return customer_address;
    }

    public void setCustomer_address(String customer_address) {
        this.customer_address = customer_address;
    }

    public String getCustomer_telp() {
        return customer_telp;
    }

    public void setCustomer_telp(String customer_telp) {
        this.customer_telp = customer_telp;
    }
}
