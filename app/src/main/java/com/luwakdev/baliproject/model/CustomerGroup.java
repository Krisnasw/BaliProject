package com.luwakdev.baliproject.model;

public class CustomerGroup {

    public CustomerGroup() {

    }

    public String customer_group_id;
    public String customer_group_name;

    public CustomerGroup(String customer_group_id, String customer_group_name) {
        this.customer_group_id = customer_group_id;
        this.customer_group_name = customer_group_name;
    }

    public String getCustomer_group_id() {
        return customer_group_id;
    }

    public void setCustomer_group_id(String customer_group_id) {
        this.customer_group_id = customer_group_id;
    }

    public String getCustomer_group_name() {
        return customer_group_name;
    }

    public void setCustomer_group_name(String customer_group_name) {
        this.customer_group_name = customer_group_name;
    }

}
