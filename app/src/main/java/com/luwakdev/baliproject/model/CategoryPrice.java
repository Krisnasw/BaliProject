package com.luwakdev.baliproject.model;

public class CategoryPrice {

    public CategoryPrice() {

    }

    public String category_price_id;
    public String category_price_name;

    public CategoryPrice(String category_price_id, String category_price_name) {
        this.category_price_id = category_price_id;
        this.category_price_name = category_price_name;
    }

    public String getCategory_price_id() {
        return category_price_id;
    }

    public void setCategory_price_id(String category_price_id) {
        this.category_price_id = category_price_id;
    }

    public String getCategory_price_name() {
        return category_price_name;
    }

    public void setCategory_price_name(String category_price_name) {
        this.category_price_name = category_price_name;
    }
}
