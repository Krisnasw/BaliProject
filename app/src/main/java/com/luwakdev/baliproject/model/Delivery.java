package com.luwakdev.baliproject.model;

public class Delivery {

    public Delivery() {

    }

    private Integer delivery_id;
    private String delivery_date;
    private Integer employee_id;
    private Integer nota_id;
    private Integer delivery_cost;
    private Integer delifery_freight_cost;
    private Integer delivery_lock;
    private Integer delivery_request;
    private Integer delivery_detail_id;
    private String delivery_detail_code;
    private Integer delivery_detail_status;
    private Integer warehouse_id;
    private Integer delivery_detail_type;
    private String delivery_detail_date;
    private String warehouse_name;
    private String warehouse_telp;
    private String warehouse_address;
    private String warehouse_fax;
    private String warehouse_pic;
    private String warehouse_code;

    public Delivery(Integer delivery_id, String delivery_date, Integer employee_id,
                    Integer nota_id, Integer delivery_cost, Integer delifery_freight_cost,
                    Integer delivery_lock, Integer delivery_request, Integer delivery_detail_id,
                    String delivery_detail_code, Integer delivery_detail_status, Integer warehouse_id,
                    Integer delivery_detail_type, String delivery_detail_date, String warehouse_name,
                    String warehouse_telp, String warehouse_address, String warehouse_fax, String warehouse_pic,
                    String warehouse_code) {

        this.delivery_id = delivery_id;
        this.delivery_date = delivery_date;
        this.employee_id = employee_id;
        this.nota_id = nota_id;
        this.delivery_cost = delivery_cost;
        this.delifery_freight_cost = delifery_freight_cost;
        this.delivery_lock = delivery_lock;
        this.delivery_request = delivery_request;
        this.delivery_detail_id = delivery_detail_id;
        this.delivery_detail_code = delivery_detail_code;
        this.delivery_detail_status = delivery_detail_status;
        this.warehouse_id = warehouse_id;
        this.delivery_detail_type = delivery_detail_type;
        this.delivery_detail_date = delivery_detail_date;
        this.warehouse_name = warehouse_name;
        this.warehouse_telp = warehouse_telp;
        this.warehouse_address = warehouse_address;
        this.warehouse_fax = warehouse_fax;
        this.warehouse_pic = warehouse_pic;
        this.warehouse_code = warehouse_code;

    }

    public Integer getDelivery_id() {
        return delivery_id;
    }

    public void setDelivery_id(Integer delivery_id) {
        this.delivery_id = delivery_id;
    }

    public String getDelivery_date() {
        return delivery_date;
    }

    public void setDelivery_date(String delivery_date) {
        this.delivery_date = delivery_date;
    }

    public Integer getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(Integer employee_id) {
        this.employee_id = employee_id;
    }

    public Integer getNota_id() {
        return nota_id;
    }

    public void setNota_id(Integer nota_id) {
        this.nota_id = nota_id;
    }

    public Integer getDelivery_cost() {
        return delivery_cost;
    }

    public void setDelivery_cost(Integer delivery_cost) {
        this.delivery_cost = delivery_cost;
    }

    public Integer getDelifery_freight_cost() {
        return delifery_freight_cost;
    }

    public void setDelifery_freight_cost(Integer delifery_freight_cost) {
        this.delifery_freight_cost = delifery_freight_cost;
    }

    public Integer getDelivery_lock() {
        return delivery_lock;
    }

    public void setDelivery_lock(Integer delivery_lock) {
        this.delivery_lock = delivery_lock;
    }

    public Integer getDelivery_request() {
        return delivery_request;
    }

    public void setDelivery_request(Integer delivery_request) {
        this.delivery_request = delivery_request;
    }

    public Integer getDelivery_detail_id() {
        return delivery_detail_id;
    }

    public void setDelivery_detail_id(Integer delivery_detail_id) {
        this.delivery_detail_id = delivery_detail_id;
    }

    public String getDelivery_detail_code() {
        return delivery_detail_code;
    }

    public void setDelivery_detail_code(String delivery_detail_code) {
        this.delivery_detail_code = delivery_detail_code;
    }

    public Integer getDelivery_detail_status() {
        return delivery_detail_status;
    }

    public void setDelivery_detail_status(Integer delivery_detail_status) {
        this.delivery_detail_status = delivery_detail_status;
    }

    public Integer getWarehouse_id() {
        return warehouse_id;
    }

    public void setWarehouse_id(Integer warehouse_id) {
        this.warehouse_id = warehouse_id;
    }

    public Integer getDelivery_detail_type() {
        return delivery_detail_type;
    }

    public void setDelivery_detail_type(Integer delivery_detail_type) {
        this.delivery_detail_type = delivery_detail_type;
    }

    public String getDelivery_detail_date() {
        return delivery_detail_date;
    }

    public void setDelivery_detail_date(String delivery_detail_date) {
        this.delivery_detail_date = delivery_detail_date;
    }

    public String getWarehouse_name() {
        return warehouse_name;
    }

    public void setWarehouse_name(String warehouse_name) {
        this.warehouse_name = warehouse_name;
    }

    public String getWarehouse_telp() {
        return warehouse_telp;
    }

    public void setWarehouse_telp(String warehouse_telp) {
        this.warehouse_telp = warehouse_telp;
    }

    public String getWarehouse_address() {
        return warehouse_address;
    }

    public void setWarehouse_address(String warehouse_address) {
        this.warehouse_address = warehouse_address;
    }

    public String getWarehouse_fax() {
        return warehouse_fax;
    }

    public void setWarehouse_fax(String warehouse_fax) {
        this.warehouse_fax = warehouse_fax;
    }

    public String getWarehouse_pic() {
        return warehouse_pic;
    }

    public void setWarehouse_pic(String warehouse_pic) {
        this.warehouse_pic = warehouse_pic;
    }

    public String getWarehouse_code() {
        return warehouse_code;
    }

    public void setWarehouse_code(String warehouse_code) {
        this.warehouse_code = warehouse_code;
    }
}
