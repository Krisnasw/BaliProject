package com.luwakdev.baliproject.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApiCustomerGroup {

    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<CustomerGroup> customerGroupList;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<CustomerGroup> getCustomerGroupList() {
        return customerGroupList;
    }

    public void setCustomerGroupList(List<CustomerGroup> customerGroupList) {
        this.customerGroupList = customerGroupList;
    }
}
