package com.luwakdev.baliproject.model;

public class Warehouse {

    public Warehouse() {

    }

    private String warehouse_id;
    private String warehouse_name;
    private String warehouse_telp;
    private String warehouse_address;
    private String warehouse_fax;
    private String warehouse_pic;
    private String warehouse_code;

    public Warehouse(String warehouse_id, String warehouse_name, String warehouse_telp, String warehouse_address, String warehouse_fax,
                     String warehouse_pic, String warehouse_code) {

        this.warehouse_id = warehouse_id;
        this.warehouse_name = warehouse_name;
        this.warehouse_telp = warehouse_telp;
        this.warehouse_address = warehouse_address;
        this.warehouse_fax = warehouse_fax;
        this.warehouse_pic = warehouse_pic;
        this.warehouse_code = warehouse_code;

    }

    public String getWarehouse_id() {
        return warehouse_id;
    }

    public void setWarehouse_id(String warehouse_id) {
        this.warehouse_id = warehouse_id;
    }

    public String getWarehouse_name() {
        return warehouse_name;
    }

    public void setWarehouse_name(String warehouse_name) {
        this.warehouse_name = warehouse_name;
    }

    public String getWarehouse_telp() {
        return warehouse_telp;
    }

    public void setWarehouse_telp(String warehouse_telp) {
        this.warehouse_telp = warehouse_telp;
    }

    public String getWarehouse_address() {
        return warehouse_address;
    }

    public void setWarehouse_address(String warehouse_address) {
        this.warehouse_address = warehouse_address;
    }

    public String getWarehouse_fax() {
        return warehouse_fax;
    }

    public void setWarehouse_fax(String warehouse_fax) {
        this.warehouse_fax = warehouse_fax;
    }

    public String getWarehouse_pic() {
        return warehouse_pic;
    }

    public void setWarehouse_pic(String warehouse_pic) {
        this.warehouse_pic = warehouse_pic;
    }

    public String getWarehouse_code() {
        return warehouse_code;
    }

    public void setWarehouse_code(String warehouse_code) {
        this.warehouse_code = warehouse_code;
    }
}
