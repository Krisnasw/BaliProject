package com.luwakdev.baliproject.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApiKonfirmasi {

    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("namaCustomer")
    @Expose
    private String namaCustomer;
    @SerializedName("alamatCustomer")
    @Expose
    private String alamatCustomer;
    @SerializedName("hargaOngkir")
    @Expose
    private String hargaOngkir;
    @SerializedName("tanggalDo")
    @Expose
    private String tanggalDo;
    @SerializedName("namaSupir")
    @Expose
    private String namaSupir;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("item")
    @Expose
    private List<Item> itemList;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getNamaCustomer() {
        return namaCustomer;
    }

    public void setNamaCustomer(String namaCustomer) {
        this.namaCustomer = namaCustomer;
    }

    public String getAlamatCustomer() {
        return alamatCustomer;
    }

    public void setAlamatCustomer(String alamatCustomer) {
        this.alamatCustomer = alamatCustomer;
    }

    public String getHargaOngkir() {
        return hargaOngkir;
    }

    public void setHargaOngkir(String hargaOngkir) {
        this.hargaOngkir = hargaOngkir;
    }

    public String getTanggalDo() {
        return tanggalDo;
    }

    public void setTanggalDo(String tanggalDo) {
        this.tanggalDo = tanggalDo;
    }

    public String getNamaSupir() {
        return namaSupir;
    }

    public void setNamaSupir(String namaSupir) {
        this.namaSupir = namaSupir;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Item> getItemList() {
        return itemList;
    }

    public void setItemList(List<Item> itemList) {
        this.itemList = itemList;
    }
}
