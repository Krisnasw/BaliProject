package com.luwakdev.baliproject.model;

import com.google.gson.annotations.SerializedName;

public class Barang {

    public Barang() {

    }

    @SerializedName("item_id")
    private Integer item_id;
    @SerializedName("item_clas_id")
    private Integer item_clas_id;
    @SerializedName("item_sub_clas_id")
    private Integer item_sub_clas_id;
    @SerializedName("item_name")
    private String item_name;
    @SerializedName("brand_id")
    private Integer brand_id;
    @SerializedName("item_barcode")
    private String item_barcode;
    @SerializedName("unit_id")
    private Integer unit_id;
    @SerializedName("item_per_unit")
    private Integer item_per_unit;
    @SerializedName("item_min")
    private Integer item_min;
    @SerializedName("item_max")
    private Integer item_max;
    @SerializedName("item_last_price")
    private Integer item_last_price;
    @SerializedName("item_price1")
    private Integer item_price1;
    @SerializedName("item_price2")
    private Integer item_price2;
    @SerializedName("item_price3")
    private Integer item_price3;
    @SerializedName("item_price4")
    private Integer item_price4;
    @SerializedName("item_price5")
    private Integer item_price5;
    @SerializedName("item_cost")
    private Integer item_cost;
    @SerializedName("item_type_id")
    private Integer item_type_id;
    @SerializedName("item_weight")
    private Integer item_weight;

    public Barang(Integer item_id, Integer item_clas_id, Integer item_sub_clas_id, String item_name, Integer brand_id, String item_barcode,
                  Integer unit_id, Integer item_per_unit, Integer item_min, Integer item_max, Integer item_last_price, Integer item_price1,
                  Integer item_price2, Integer item_price3, Integer item_price4, Integer item_price5, Integer item_cost, Integer item_type_id,
                  Integer item_weight) {

        this.item_id = item_id;
        this.item_clas_id = item_clas_id;
        this.item_sub_clas_id = item_sub_clas_id;
        this.item_name = item_name;
        this.brand_id = brand_id;
        this.item_barcode = item_barcode;
        this.unit_id = unit_id;
        this.item_per_unit = item_per_unit;
        this.item_min = item_min;
        this.item_max = item_max;
        this.item_last_price = item_last_price;
        this.item_price1 = item_price1;
        this.item_price2 = item_price2;
        this.item_price3 = item_price3;
        this.item_price4 = item_price4;
        this.item_price5 = item_price5;
        this.item_cost = item_cost;
        this.item_type_id = item_type_id;
        this.item_weight = item_weight;

    }

    public Integer getItem_id() {
        return item_id;
    }

    public void setItem_id(Integer item_id) {
        this.item_id = item_id;
    }

    public Integer getItem_clas_id() {
        return item_clas_id;
    }

    public void setItem_clas_id(Integer item_clas_id) {
        this.item_clas_id = item_clas_id;
    }

    public Integer getItem_sub_clas_id() {
        return item_sub_clas_id;
    }

    public void setItem_sub_clas_id(Integer item_sub_clas_id) {
        this.item_sub_clas_id = item_sub_clas_id;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public Integer getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(Integer brand_id) {
        this.brand_id = brand_id;
    }

    public String getItem_barcode() {
        return item_barcode;
    }

    public void setItem_barcode(String item_barcode) {
        this.item_barcode = item_barcode;
    }

    public Integer getUnit_id() {
        return unit_id;
    }

    public void setUnit_id(Integer unit_id) {
        this.unit_id = unit_id;
    }

    public Integer getItem_per_unit() {
        return item_per_unit;
    }

    public void setItem_per_unit(Integer item_per_unit) {
        this.item_per_unit = item_per_unit;
    }

    public Integer getItem_min() {
        return item_min;
    }

    public void setItem_min(Integer item_min) {
        this.item_min = item_min;
    }

    public Integer getItem_max() {
        return item_max;
    }

    public void setItem_max(Integer item_max) {
        this.item_max = item_max;
    }

    public Integer getItem_last_price() {
        return item_last_price;
    }

    public void setItem_last_price(Integer item_last_price) {
        this.item_last_price = item_last_price;
    }

    public Integer getItem_price1() {
        return item_price1;
    }

    public void setItem_price1(Integer item_price1) {
        this.item_price1 = item_price1;
    }

    public Integer getItem_price2() {
        return item_price2;
    }

    public void setItem_price2(Integer item_price2) {
        this.item_price2 = item_price2;
    }

    public Integer getItem_price3() {
        return item_price3;
    }

    public void setItem_price3(Integer item_price3) {
        this.item_price3 = item_price3;
    }

    public Integer getItem_price4() {
        return item_price4;
    }

    public void setItem_price4(Integer item_price4) {
        this.item_price4 = item_price4;
    }

    public Integer getItem_price5() {
        return item_price5;
    }

    public void setItem_price5(Integer item_price5) {
        this.item_price5 = item_price5;
    }

    public Integer getItem_cost() {
        return item_cost;
    }

    public void setItem_cost(Integer item_cost) {
        this.item_cost = item_cost;
    }

    public Integer getItem_type_id() {
        return item_type_id;
    }

    public void setItem_type_id(Integer item_type_id) {
        this.item_type_id = item_type_id;
    }

    public Integer getItem_weight() {
        return item_weight;
    }

    public void setItem_weight(Integer item_weight) {
        this.item_weight = item_weight;
    }
}
