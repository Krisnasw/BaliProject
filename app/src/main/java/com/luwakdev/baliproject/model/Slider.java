package com.luwakdev.baliproject.model;

public class Slider {

    public Slider() {

    }

    private String item_galery_file;

    public Slider(String item_galery_file) {
        this.item_galery_file = item_galery_file;
    }

    public String getItem_galery_file() {
        return item_galery_file;
    }

    public void setItem_galery_file(String item_galery_file) {
        this.item_galery_file = item_galery_file;
    }

}
