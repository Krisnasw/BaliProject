package com.luwakdev.baliproject.model;

public class Item {

    public Item() {

    }

    private Integer delivery_send_id;
    private Integer delivery_detail_id;
    private Integer nota_detail_order_id;
    private Integer delivery_send_qty;
    private String item_name;
    private Integer item_per_unit;
    private String unit_name;
    private Integer foreman_detail_qty;
    private Integer nota_detail_order_qty;
    private Integer nota_detail_order_now;

    public Item(Integer delivery_send_id, Integer delivery_detail_id, Integer nota_detail_order_id,
                Integer delivery_send_qty, String item_name, Integer item_per_unit,
                String unit_name, Integer foreman_detail_qty, Integer nota_detail_order_qty, Integer nota_detail_order_now) {

        this.delivery_send_id = delivery_send_id;
        this.delivery_detail_id = delivery_detail_id;
        this.nota_detail_order_id = nota_detail_order_id;
        this.delivery_send_qty = delivery_send_qty;
        this.item_name = item_name;
        this.item_per_unit = item_per_unit;
        this.unit_name = unit_name;
        this.foreman_detail_qty = foreman_detail_qty;
        this.nota_detail_order_qty = nota_detail_order_qty;
        this.nota_detail_order_now = nota_detail_order_now;

    }

    public Integer getDelivery_send_id() {
        return delivery_send_id;
    }

    public void setDelivery_send_id(Integer delivery_send_id) {
        this.delivery_send_id = delivery_send_id;
    }

    public Integer getDelivery_detail_id() {
        return delivery_detail_id;
    }

    public void setDelivery_detail_id(Integer delivery_detail_id) {
        this.delivery_detail_id = delivery_detail_id;
    }

    public Integer getNota_detail_order_id() {
        return nota_detail_order_id;
    }

    public void setNota_detail_order_id(Integer nota_detail_order_id) {
        this.nota_detail_order_id = nota_detail_order_id;
    }

    public Integer getDelivery_send_qty() {
        return delivery_send_qty;
    }

    public void setDelivery_send_qty(Integer delivery_send_qty) {
        this.delivery_send_qty = delivery_send_qty;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public Integer getItem_per_unit() {
        return item_per_unit;
    }

    public void setItem_per_unit(Integer item_per_unit) {
        this.item_per_unit = item_per_unit;
    }

    public String getUnit_name() {
        return unit_name;
    }

    public void setUnit_name(String unit_name) {
        this.unit_name = unit_name;
    }

    public Integer getForeman_detail_qty() {
        return foreman_detail_qty;
    }

    public void setForeman_detail_qty(Integer foreman_detail_qty) {
        this.foreman_detail_qty = foreman_detail_qty;
    }

    public Integer getNota_detail_order_qty() {
        return nota_detail_order_qty;
    }

    public void setNota_detail_order_qty(Integer nota_detail_order_qty) {
        this.nota_detail_order_qty = nota_detail_order_qty;
    }

    public Integer getNota_detail_order_now() {
        return nota_detail_order_now;
    }

    public void setNota_detail_order_now(Integer nota_detail_order_now) {
        this.nota_detail_order_now = nota_detail_order_now;
    }
}
