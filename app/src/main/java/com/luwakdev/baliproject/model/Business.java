package com.luwakdev.baliproject.model;

public class Business {

    public Business() {

    }

    public String busines_id;
    public String busines_name;

    public Business(String busines_id, String busines_name) {
        this.busines_id = busines_id;
        this.busines_name = busines_name;
    }

    public String getBusines_id() {
        return busines_id;
    }

    public void setBusines_id(String busines_id) {
        this.busines_id = busines_id;
    }

    public String getBusines_name() {
        return busines_name;
    }

    public void setBusines_name(String busines_name) {
        this.busines_name = busines_name;
    }
}
