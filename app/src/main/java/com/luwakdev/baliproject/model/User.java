package com.luwakdev.baliproject.model;

public class User {

    public User() {

    }

    private Integer user_id;
    private Integer user_type_id;
    private String user_username;
    private String user_name;
    private String user_address;
    private String user_ktp;
    private String user_img;
    private Integer user_active_status;
    private Integer employee_id;

    public User(Integer user_id, Integer user_type_id, String user_username, String user_name, String user_address, String user_ktp,
                String user_img, Integer user_active_status, Integer employee_id) {

        this.user_id = user_id;
        this.user_type_id = user_type_id;
        this.user_username = user_username;
        this.user_name = user_name;
        this.user_address = user_address;
        this.user_ktp = user_ktp;
        this.user_img = user_img;
        this.user_active_status = user_active_status;
        this.employee_id = employee_id;

    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public Integer getUser_type_id() {
        return user_type_id;
    }

    public void setUser_type_id(Integer user_type_id) {
        this.user_type_id = user_type_id;
    }

    public String getUser_username() {
        return user_username;
    }

    public void setUser_username(String user_username) {
        this.user_username = user_username;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_address() {
        return user_address;
    }

    public void setUser_address(String user_address) {
        this.user_address = user_address;
    }

    public String getUser_ktp() {
        return user_ktp;
    }

    public void setUser_ktp(String user_ktp) {
        this.user_ktp = user_ktp;
    }

    public String getUser_img() {
        return user_img;
    }

    public void setUser_img(String user_img) {
        this.user_img = user_img;
    }

    public Integer getUser_active_status() {
        return user_active_status;
    }

    public void setUser_active_status(Integer user_active_status) {
        this.user_active_status = user_active_status;
    }

    public Integer getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(Integer employee_id) {
        this.employee_id = employee_id;
    }
}
