package com.luwakdev.baliproject.view.stok;

import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.luwakdev.baliproject.R;
import com.luwakdev.baliproject.model.ApiBarang;
import com.luwakdev.baliproject.model.Barang;
import com.luwakdev.baliproject.service.ApiClient;
import com.luwakdev.baliproject.view.main.MainActivity;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class StokFragment extends Fragment {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.search_view)
    MaterialSearchView materialSearchView;
    @BindView(R.id.rvStok)
    RecyclerView recyclerView;

    private SweetAlertDialog sweetAlertDialog;
    private String title = "";

    private static final String TAG = StokFragment.class.getSimpleName();

    public StokFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_stok, container, false);

        ButterKnife.bind(this, view);

        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Pencarian Barang Disini");

        sweetAlertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        sweetAlertDialog.setContentText("Please wait...");
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.show();

        getListBarang();

        materialSearchView.setVoiceSearch(true);
        materialSearchView.setHint("Masukkan Nama Barang Yang Dicari");
        materialSearchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                title = query;
                if (title.isEmpty()) {
                    return false;
                } else {
                    sweetAlertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
                    sweetAlertDialog.setContentText("Please wait...");
                    sweetAlertDialog.show();
                    getSearchBarang(title);
                    return false;
                }
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                title = newText;
                if (title.isEmpty()) {
                    return false;
                } else {
                    sweetAlertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
                    sweetAlertDialog.setContentText("Please wait...");
                    sweetAlertDialog.show();
                    getSearchBarang(title);
                    return false;
                }
            }
        });

        materialSearchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {

            }

            @Override
            public void onSearchViewClosed() {

            }
        });

        swipeRefreshLayout.setColorSchemeResources(R.color.light_blue, R.color.green, R.color.red);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshContent();
            }
        });

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MaterialSearchView.REQUEST_VOICE && resultCode == RESULT_OK) {
            ArrayList<String> matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            if (matches != null && matches.size() > 0) {
                String searchWrd = matches.get(0);
                if (!TextUtils.isEmpty(searchWrd)) {
                    materialSearchView.setQuery(searchWrd, false);
                }
            }

            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main_menu, menu);
        super.onCreateOptionsMenu(menu,inflater);

        MenuItem item = menu.findItem(R.id.action_search);
        materialSearchView.setMenuItem(item);
    }

    private void refreshContent() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        ApiClient.get(getActivity()).getListBarang().enqueue(new Callback<ApiBarang>() {
            @Override
            public void onResponse(Call<ApiBarang> call, Response<ApiBarang> response) {
                if (response.body().getError() == "false") {
                    sweetAlertDialog.dismissWithAnimation();
                    List<Barang> barangs = response.body().getBarangs();
                    Log.e(TAG, "Jumlah Barang : "+barangs.size());
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(new StokAdapter(barangs, R.layout.stok_item, getActivity()));
                    swipeRefreshLayout.setRefreshing(false);
                } else {
                    sweetAlertDialog.dismissWithAnimation();
                    new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Notification")
                            .setContentText(response.body().getMessage())
                            .show();
                }
            }

            @Override
            public void onFailure(Call<ApiBarang> call, Throwable t) {
                try {
                    throw new InterruptedException("Gagal Connect Server");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void getListBarang() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        ApiClient.get(getActivity()).getListBarang().enqueue(new Callback<ApiBarang>() {
            @Override
            public void onResponse(Call<ApiBarang> call, Response<ApiBarang> response) {
                if (response.body().getError() == "false") {
                    sweetAlertDialog.dismissWithAnimation();
                    List<Barang> barangs = response.body().getBarangs();
                    Log.e(TAG, "Jumlah Barang : "+barangs.size());
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(new StokAdapter(barangs, R.layout.stok_item, getActivity()));
                } else {
                    sweetAlertDialog.dismissWithAnimation();
                    new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Notification")
                            .setContentText(response.body().getMessage())
                            .show();
                }
            }

            @Override
            public void onFailure(Call<ApiBarang> call, Throwable t) {
                try {
                    throw new InterruptedException("Gagal Connect Server");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void getSearchBarang(String keys) {
        ApiClient.get(getActivity()).getSearchBarang(keys).enqueue(new Callback<ApiBarang>() {
            @Override
            public void onResponse(Call<ApiBarang> call, Response<ApiBarang> response) {
                ApiBarang resp = response.body();
                if (resp.getError() == "false") {
                    sweetAlertDialog.dismissWithAnimation();
                    List<Barang> barangs = response.body().getBarangs();
                    Log.e(TAG, "Jumlah Barang : "+barangs.size());
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(new StokAdapter(barangs, R.layout.stok_item, getActivity()));
                } else {
                    sweetAlertDialog.dismissWithAnimation();
                    new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Notification")
                            .setContentText(resp.getMessage())
                            .show();
                }
            }

            @Override
            public void onFailure(Call<ApiBarang> call, Throwable t) {
                try {
                    throw new InterruptedException("Gagal Connect Server");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
