package com.luwakdev.baliproject.view.main;

import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.luwakdev.baliproject.R;
import com.luwakdev.baliproject.model.ApiUser;
import com.luwakdev.baliproject.model.User;
import com.luwakdev.baliproject.service.ApiClient;
import com.luwakdev.baliproject.utils.AppConfig;
import com.luwakdev.baliproject.view.about.AboutFragment;
import com.luwakdev.baliproject.view.customer.CustomerFragment;
import com.luwakdev.baliproject.view.konfirmasi.ConfirmFragment;
import com.luwakdev.baliproject.view.login.LoginActivity;
import com.luwakdev.baliproject.view.pelunasan.PelunasanFragment;
import com.luwakdev.baliproject.view.piutang.PiutangFragment;
import com.luwakdev.baliproject.view.stok.StokFragment;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.pixplicity.easyprefs.library.Prefs;

import java.io.UnsupportedEncodingException;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static java.lang.System.exit;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.content)
    FrameLayout content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Bali System");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (Prefs.getString(AppConfig.KEY_USERNAME, "") != null) {
            getUserData();
        } else {
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
            finish();
        }

        setupDrawer();
        attachFragment(new MainFragment());
    }

    public void setupDrawer() {
        AccountHeader accountHeader = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.drawable.header)
                .addProfiles(
                        new ProfileDrawerItem().withName("Demo").withEmail(Prefs.getString(AppConfig.KEY_USERNAME, "")).withIcon(R.drawable.user)
                )
                .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
                    @Override
                    public boolean onProfileChanged(View view, IProfile profile, boolean current) {
                        return false;
                    }
                })
                .build();

        new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withAccountHeader(accountHeader)
                .addDrawerItems(
                        new PrimaryDrawerItem().withIdentifier(1).withName("Dashboard").withIcon(GoogleMaterial.Icon.gmd_home),
                        new PrimaryDrawerItem().withIdentifier(2).withName("Pengolahan Stok").withIcon(GoogleMaterial.Icon.gmd_add_box),
                        new PrimaryDrawerItem().withIdentifier(3).withName("Konfirmasi DO").withIcon(GoogleMaterial.Icon.gmd_confirmation_number),
                        new PrimaryDrawerItem().withIdentifier(4).withName("Tambah Customer").withIcon(GoogleMaterial.Icon.gmd_people),
                        new PrimaryDrawerItem().withIdentifier(6).withName("Cek Pelunasan").withIcon(GoogleMaterial.Icon.gmd_assignment_turned_in),
                        new PrimaryDrawerItem().withIdentifier(7).withName("Cek Piutang").withIcon(GoogleMaterial.Icon.gmd_attach_money),
                        new PrimaryDrawerItem().withIdentifier(5).withName("Tentang Kami").withIcon(GoogleMaterial.Icon.gmd_contacts)
                )
                .addStickyDrawerItems(
                        new PrimaryDrawerItem().withIdentifier(10).withName("Logout").withIcon(GoogleMaterial.Icon.gmd_exit_to_app)
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        switch ((int) drawerItem.getIdentifier()) {
                            case 1:
                                attachFragment(new MainFragment());
                                return false;
                            case 2:
                                attachFragment(new StokFragment());
                                return false;
                            case 3:
                                attachFragment(new ConfirmFragment());
                                return false;
                            case 4:
                                attachFragment(new CustomerFragment());
                                return false;
                            case 5:
                                attachFragment(new AboutFragment());
                                return false;
                            case 6:
                                attachFragment(new PelunasanFragment());
                                return false;
                            case 7:
                                attachFragment(new PiutangFragment());
                                return false;
                            case 10:
                                Prefs.clear();
                                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                                finish();
                                return false;
                            default:
                                attachFragment(new MainFragment());
                                return false;
                        }
                    }
                })
                .build();
    }

    public void attachFragment(Fragment fragment) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content, fragment, "main_fragment");
        ft.addToBackStack(null);
        ft.commitAllowingStateLoss();
    }

    @Override
    public void onBackPressed() {
        new SweetAlertDialog(MainActivity.this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Notification")
                .setContentText("Keluar Aplikasi?")
                .setConfirmText("Ya")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        Prefs.clear();
                        finish();
                        exit(0);
                    }
                })
                .setCancelText("Tidak")
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismissWithAnimation();
                    }
                })
                .show();
    }

    private void getUserData() {
        byte[] data = new byte[0];
        try {
            data = String.valueOf(Prefs.getString(AppConfig.KEY_USERNAME, "")).getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String base64 = Base64.encodeToString(data, Base64.DEFAULT);

        ApiClient.get(this).getUserData(base64).enqueue(new Callback<ApiUser>() {
            @Override
            public void onResponse(Call<ApiUser> call, Response<ApiUser> response) {
                ApiUser resp = response.body();

                if (resp.getError() == "false") {
                    Toast.makeText(MainActivity.this, "User Ditemukan", Toast.LENGTH_SHORT).show();
                } else {
                    new SweetAlertDialog(MainActivity.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Info")
                            .setContentText(resp.getMessage())
                            .show();
                }
            }

            @Override
            public void onFailure(Call<ApiUser> call, Throwable t) {
                try {
                    throw new InterruptedException("Gagal Connect Server");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}
