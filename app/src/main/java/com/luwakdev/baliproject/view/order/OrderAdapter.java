package com.luwakdev.baliproject.view.order;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Toast;

import com.luwakdev.baliproject.R;
import com.luwakdev.baliproject.model.ApiCart;
import com.luwakdev.baliproject.model.Item;
import com.luwakdev.baliproject.service.ApiClient;
import com.luwakdev.baliproject.utils.AppConfig;
import com.luwakdev.baliproject.utils.MyEditText;
import com.luwakdev.baliproject.utils.MyTextView;
import com.pixplicity.easyprefs.library.Prefs;
import com.shawnlin.numberpicker.NumberPicker;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.MyViewHolder> {

    private List<Item> items;
    private int rowLayout;
    private Context context;
//    SweetAlertDialog sweetAlertDialog;

    Integer quantity;

    public OrderAdapter(Context context) {
        this.context = context;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image)
        ImageView imageView;
        @BindView(R.id.title)
        MyTextView title;
        @BindView(R.id.subTitle)
        MyTextView subTitle;
        @BindView(R.id.etQuantity)
        MyEditText quantityTxt;
        @BindView(R.id.tvQuantity)
        MyTextView tvQuantity;
        @BindView(R.id.quantityHidden)
        MyTextView hiddenQty;

        public MyViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

    }

    public OrderAdapter(List<Item> items, int rowLayout, Context context) {
        this.items = items;
        this.rowLayout = rowLayout;
        this.context = context;
    }

    @Override
    public OrderAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.title.setText(items.get(position).getItem_name());
        holder.subTitle.setText(items.get(position).getUnit_name());
        holder.tvQuantity.setText(Integer.toString(items.get(position).getNota_detail_order_qty()));

        if (Prefs.getString(AppConfig.KEY_CONFIRM, "").equals("1")) {
            holder.quantityTxt.setVisibility(View.GONE);
            holder.hiddenQty.setVisibility(View.VISIBLE);
            holder.hiddenQty.setText(Integer.toString(items.get(position).getNota_detail_order_now()));
        } else {
            holder.hiddenQty.setVisibility(View.GONE);
            holder.quantityTxt.setVisibility(View.VISIBLE);
            holder.quantityTxt.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    updateDeliveredQty(items.get(position).getNota_detail_order_id(), holder.quantityTxt.getText().toString());
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    private void updateDeliveredQty(Integer orderId, String qty) {
        ApiClient.get(context).postUpdateCart(orderId, qty).enqueue(new Callback<ApiCart>() {
            @Override
            public void onResponse(Call<ApiCart> call, Response<ApiCart> response) {
                ApiCart resp = response.body();

                if (resp.getError() == "false") {
//                    sweetAlertDialog.dismissWithAnimation();
                } else {
//                    sweetAlertDialog.dismissWithAnimation();
//                    new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
//                            .setTitleText("Notification")
//                            .setContentText(resp.getMessage())
//                            .show();
                }
            }

            @Override
            public void onFailure(Call<ApiCart> call, Throwable t) {
                try {
                    new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Error")
                            .setContentText(t.getMessage())
                            .show();
                    throw new InterruptedException("Terjadi Kesalahan");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
