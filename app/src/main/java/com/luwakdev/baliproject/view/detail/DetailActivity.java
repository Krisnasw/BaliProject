package com.luwakdev.baliproject.view.detail;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterViewFlipper;

import com.luwakdev.baliproject.R;
import com.luwakdev.baliproject.model.ApiSlider;
import com.luwakdev.baliproject.model.Slider;
import com.luwakdev.baliproject.service.ApiClient;
import com.luwakdev.baliproject.utils.AppConfig;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    Integer id;

    @BindView(R.id.adapterViewFlipper)
    AdapterViewFlipper adapterViewFlipper;

    HashMap<Integer, String> file_maps;

    SweetAlertDialog sweetAlertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_barang);

        ButterKnife.bind(this);

        id = getIntent().getExtras().getInt("id");

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Bali System");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        sweetAlertDialog = new SweetAlertDialog(DetailActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        sweetAlertDialog.setContentText("Please wait...");
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.show();

        initSlider();
    }

    private void initSlider() {
        ApiClient.get(this).getSliderList(id).enqueue(new Callback<ApiSlider>() {
            @Override
            public void onResponse(Call<ApiSlider> call, Response<ApiSlider> response) {
                ApiSlider resp = response.body();

                if (resp.getError() == "false") {
                    sweetAlertDialog.dismissWithAnimation();
                    List<Slider> sliders = resp.getSliderList();

                    FlipperAdapter adapter = new FlipperAdapter(DetailActivity.this, sliders);

                    adapterViewFlipper.setAdapter(adapter);
                    adapterViewFlipper.setFlipInterval(1000);
                    adapterViewFlipper.startFlipping();
                } else {
                    sweetAlertDialog.dismissWithAnimation();
                    new SweetAlertDialog(DetailActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Error")
                            .setContentText(resp.getMessage())
                            .show();
                }
            }

            @Override
            public void onFailure(Call<ApiSlider> call, Throwable t) {
                try {
                    new SweetAlertDialog(DetailActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Error")
                            .setContentText(t.getMessage())
                            .show();
                    throw new InterruptedException("Terjadi Kesalahan");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
