package com.luwakdev.baliproject.view.order;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.luwakdev.baliproject.R;
import com.luwakdev.baliproject.model.ApiConfirmed;
import com.luwakdev.baliproject.model.ApiKonfirmasi;
import com.luwakdev.baliproject.model.Delivery;
import com.luwakdev.baliproject.model.Item;
import com.luwakdev.baliproject.service.ApiClient;
import com.luwakdev.baliproject.utils.AppConfig;
import com.luwakdev.baliproject.utils.MyTextView;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderActivity extends AppCompatActivity {

    @BindView(R.id.namaSopir)
    MyTextView namaSupir;
    @BindView(R.id.tanggalDO)
    MyTextView tanggalDO;
    @BindView(R.id.namaCustomer)
    MyTextView namaCustomer;
    @BindView(R.id.biayaAngkut)
    MyTextView biayaAngkut;
    @BindView(R.id.alamat)
    MyTextView alamat;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;
    @BindView(R.id.btn)
    MyTextView btnSubmit;
    @BindView(R.id.txtConfirm)
    MyTextView confirmText;

    SweetAlertDialog sweetAlertDialog, swal;

    Integer delivery_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        ButterKnife.bind(this);

        delivery_id = getIntent().getExtras().getInt("delivery_id");

        sweetAlertDialog = new SweetAlertDialog(OrderActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        sweetAlertDialog.setContentText("Please wait...");
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.show();

        initData(delivery_id);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                swal = new SweetAlertDialog(OrderActivity.this, SweetAlertDialog.PROGRESS_TYPE);
                swal.setContentText("Please wait...");
                swal.setCancelable(false);
                swal.show();
                setConfirmedStok();
            }
        });
    }

    private void initData(final int delivery_id) {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        ApiClient.get(this).getKonfirmasiData(delivery_id).enqueue(new Callback<ApiKonfirmasi>() {
            @Override
            public void onResponse(Call<ApiKonfirmasi> call, Response<ApiKonfirmasi> response) {
                ApiKonfirmasi resp = response.body();

                if (resp.getError() == "false") {
                    sweetAlertDialog.dismissWithAnimation();

                    namaSupir.setText(resp.getNamaSupir());
                    namaCustomer.setText(resp.getNamaCustomer());
                    alamat.setText(resp.getAlamatCustomer());
                    tanggalDO.setText(resp.getTanggalDo());
                    biayaAngkut.setText(resp.getHargaOngkir());
                    confirmText.setText((resp.getStatus().equals("1")) ? "Confirmed" : "Unconfirmed");
                    Prefs.putString(AppConfig.KEY_CONFIRM, resp.getStatus());

                    if (resp.getStatus().equals("1")) {
                        btnSubmit.setBackgroundColor(Color.RED);
                        btnSubmit.setText("TERKONFIRMASI");
                        btnSubmit.setEnabled(false);
                    }

                    List<Item> itemList = resp.getItemList();
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(new OrderAdapter(itemList, R.layout.item_add_product, OrderActivity.this));
                } else {
                    sweetAlertDialog.dismissWithAnimation();
                    new SweetAlertDialog(OrderActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Error")
                            .setContentText(resp.getMessage())
                            .show();
                }
            }

            @Override
            public void onFailure(Call<ApiKonfirmasi> call, Throwable t) {
                try {
                    new SweetAlertDialog(OrderActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Error")
                            .setContentText(t.getMessage())
                            .show();
                    throw new InterruptedException("Terjadi Kesalahan");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setConfirmedStok() {
        ApiClient.get(this).postConfirmedCart(delivery_id).enqueue(new Callback<ApiConfirmed>() {
            @Override
            public void onResponse(Call<ApiConfirmed> call, Response<ApiConfirmed> response) {
                ApiConfirmed resp = response.body();

                if (resp.getError() == "false") {
                    swal.dismissWithAnimation();
                    new SweetAlertDialog(OrderActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText("Notification")
                            .setContentText(resp.getMessage())
                            .show();
                } else {
                    swal.dismissWithAnimation();
                    new SweetAlertDialog(OrderActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Notification")
                            .setContentText(resp.getMessage())
                            .show();
                }
            }

            @Override
            public void onFailure(Call<ApiConfirmed> call, Throwable t) {
                try {
                    new SweetAlertDialog(OrderActivity.this, SweetAlertDialog.PROGRESS_TYPE)
                            .setTitleText("Errror")
                            .setContentText(t.getMessage())
                            .show();
                    throw new InterruptedException("Terjadi Kesalahan");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}