package com.luwakdev.baliproject.view.piutang;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.luwakdev.baliproject.R;
import com.luwakdev.baliproject.model.Nota;
import com.luwakdev.baliproject.view.pelunasan.PelunasanAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PiutangAdapter extends RecyclerView.Adapter<PiutangAdapter.MyViewHolder> {

    private List<Nota> notaList;
    private int rowLayout;
    private Context context;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.layoutStok)
        LinearLayout linearLayout;
        @BindView(R.id.tvKodeNota)
        TextView kodeNota;
        @BindView(R.id.tvTanggal)
        TextView tanggal;
        @BindView(R.id.tvCustomer)
        TextView customer;
        @BindView(R.id.tvAlamat)
        TextView alamat;
        @BindView(R.id.tvStatus)
        TextView status;

        public MyViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

    }

    public PiutangAdapter(List<Nota> notaList, int rowLayout, Context context) {
        this.notaList = notaList;
        this.rowLayout = rowLayout;
        this.context = context;
    }

    @Override
    public PiutangAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PiutangAdapter.MyViewHolder holder, final int position) {
        holder.kodeNota.setText("Kode Nota : "+notaList.get(position).getNota_code());
        holder.tanggal.setText("Tanggal : "+notaList.get(position).getNota_date());
        holder.customer.setText("Jatuh Tempo : "+notaList.get(position).getNota_tempo());
        holder.alamat.setText("Alamat : "+notaList.get(position).getCustomer_address());
        holder.status.setText("Status : "+((notaList.get(position).getNota_status() == 0) ? "Proses" : "DO"));
    }

    @Override
    public int getItemCount() {
        return notaList.size();
    }

}
