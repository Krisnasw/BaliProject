package com.luwakdev.baliproject.view.customer;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.CursorLoader;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.luwakdev.baliproject.R;
import com.luwakdev.baliproject.model.ApiBusiness;
import com.luwakdev.baliproject.model.ApiCategoryPrice;
import com.luwakdev.baliproject.model.ApiCity;
import com.luwakdev.baliproject.model.ApiCustomer;
import com.luwakdev.baliproject.model.ApiCustomerGroup;
import com.luwakdev.baliproject.model.Business;
import com.luwakdev.baliproject.model.CategoryPrice;
import com.luwakdev.baliproject.model.City;
import com.luwakdev.baliproject.model.CustomerGroup;
import com.luwakdev.baliproject.service.ApiClient;
import com.luwakdev.baliproject.view.main.MainActivity;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Select;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomerActivity extends AppCompatActivity implements Validator.ValidationListener {

    @BindView(R.id.btnCancel)
    Button btnCancel;
    @Select
    @BindView(R.id.spinnerGroup)
    Spinner spinnerGroup;
    @Select
    @BindView(R.id.spinnerHarga)
    Spinner spinnerHarga;
    @Select
    @BindView(R.id.spinnerUsaha)
    Spinner spinnerUsaha;
    @Select
    @BindView(R.id.spinnerWilayah)
    Spinner spinnerWilayah;
    @BindView(R.id.linearImage)
    LinearLayout linearLayout;
    @BindView(R.id.showImage)
    ImageView showImage;
    @BindView(R.id.btnUpload)
    Button btnUpload;
    @NotEmpty
    @BindView(R.id.etNama)
    MaterialEditText etNama;
    @NotEmpty
    @BindView(R.id.etAlamat)
    MaterialEditText etAlamat;
    @NotEmpty
    @BindView(R.id.etNamaToko)
    MaterialEditText etNamaToko;
    @NotEmpty
    @BindView(R.id.etTelp)
    MaterialEditText etTelp;
    @NotEmpty
    @BindView(R.id.etHp)
    MaterialEditText etHp;
    @NotEmpty
    @BindView(R.id.etEmail)
    MaterialEditText etEmail;
    @NotEmpty
    @BindView(R.id.etLimitKredit)
    MaterialEditText etLimitKredit;
    @NotEmpty
    @BindView(R.id.etLimitTempo)
    MaterialEditText etLimitTempo;
    @NotEmpty
    @BindView(R.id.etNoNPWP)
    MaterialEditText etNoNPWP;
    @NotEmpty
    @BindView(R.id.etNamaNPWP)
    MaterialEditText etNamaNPWP;
    @NotEmpty
    @BindView(R.id.etAlamatNPWP)
    MaterialEditText etAlamatNPWP;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;

    File file;

    private String selectedName, selectedHarga, selectedUsaha, selectedWilayah;

    private Validator validator;
    private SweetAlertDialog swal;

    private static final int REQUEST_GALLERY_CODE = 200;
    private static final int READ_REQUEST_CODE = 300;

    private Uri uri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_customer);

        ButterKnife.bind(this);

        validator = new Validator(this);
        validator.setValidationListener(this);

        linearLayout.setVisibility(View.GONE);

        initSpinnerGroup();
        initSpinnerHarga();
        initSpinnerUsaha();
        initSpinnerWilayah();

        spinnerGroup.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedName = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerHarga.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedHarga = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerUsaha.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedUsaha = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerWilayah.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedWilayah = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CustomerActivity.this, MainActivity.class));
                finish();
            }
        });

        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent openGalleryIntent = new Intent(Intent.ACTION_PICK);
                openGalleryIntent.setType("image/*");
                startActivityForResult(openGalleryIntent, REQUEST_GALLERY_CODE);
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validator.validate();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

//    private void imageBrowse() {
//        Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//        startActivityForResult(galleryIntent, PICK_IMAGE_REQUEST);
//    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_GALLERY_CODE && resultCode == Activity.RESULT_OK){
            uri = data.getData();
            String filePath = getRealPathFromURIPath(uri, CustomerActivity.this);
            file = new File(filePath);
            Log.d(CustomerFragment.class.getSimpleName(), "Filename " + file.getName());

            linearLayout.setVisibility(View.VISIBLE);
//            Glide.with(this).load(filePath).into(showImage);
            showImage.setImageURI(data.getData());
        }
    }

    private String getRealPathFromURIPath(Uri contentURI, Activity activity) {
        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    private void initSpinnerGroup() {
        ApiClient.get(getApplicationContext()).getListCustomerGroup().enqueue(new Callback<ApiCustomerGroup>() {
            @Override
            public void onResponse(Call<ApiCustomerGroup> call, Response<ApiCustomerGroup> response) {
                ApiCustomerGroup resp = response.body();

                if (resp.getError() == "false") {
                    List<CustomerGroup> apiCustomerGroupList = resp.getCustomerGroupList();
                    List<String> stringList = new ArrayList<String>();

                    for (int i = 0; i < apiCustomerGroupList.size(); i++) {
                        stringList.add(apiCustomerGroupList.get(i).getCustomer_group_name());
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                            android.R.layout.simple_spinner_item, stringList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinnerGroup.setAdapter(adapter);
                } else {
                    new SweetAlertDialog(getApplicationContext(), SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Notif")
                            .setContentText(resp.getMessage())
                            .show();
                }
            }

            @Override
            public void onFailure(Call<ApiCustomerGroup> call, Throwable t) {
                try {
                    throw new InterruptedException("Gagal Connect Server");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void initSpinnerHarga() {
        ApiClient.get(getApplicationContext()).getListCategoryPrice().enqueue(new Callback<ApiCategoryPrice>() {
            @Override
            public void onResponse(Call<ApiCategoryPrice> call, Response<ApiCategoryPrice> response) {
                ApiCategoryPrice resp = response.body();

                if (resp.getError() == "false") {
                    List<CategoryPrice> categoryPriceList = resp.getCategoryPriceList();
                    List<String> stringList = new ArrayList<String>();

                    for (int i = 0; i < categoryPriceList.size(); i++) {
                        stringList.add(categoryPriceList.get(i).getCategory_price_name());
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                            R.layout.support_simple_spinner_dropdown_item, stringList);
                    adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                    spinnerHarga.setAdapter(adapter);
                } else {
                    new SweetAlertDialog(getApplicationContext(), SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Notif")
                            .setContentText(resp.getMessage())
                            .show();
                }
            }

            @Override
            public void onFailure(Call<ApiCategoryPrice> call, Throwable t) {
                try {
                    throw new InterruptedException("Gagal Connect Server");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void initSpinnerUsaha() {
        ApiClient.get(getApplicationContext()).getListBusiness().enqueue(new Callback<ApiBusiness>() {
            @Override
            public void onResponse(Call<ApiBusiness> call, Response<ApiBusiness> response) {
                ApiBusiness resp = response.body();

                if (resp.getError() == "false") {
                    List<Business> businessList = resp.getBusinessList();
                    List<String> stringList = new ArrayList<String>();

                    for (int i = 0; i < businessList.size(); i++) {
                        stringList.add(businessList.get(i).getBusines_name());
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                            R.layout.support_simple_spinner_dropdown_item, stringList);
                    adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                    spinnerUsaha.setAdapter(adapter);
                } else {
                    new SweetAlertDialog(getApplicationContext(), SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Notif")
                            .setContentText(resp.getMessage())
                            .show();
                }
            }

            @Override
            public void onFailure(Call<ApiBusiness> call, Throwable t) {
                try {
                    throw new InterruptedException("Gagal Connect Server");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void initSpinnerWilayah() {
        ApiClient.get(getApplicationContext()).getListCity().enqueue(new Callback<ApiCity>() {
            @Override
            public void onResponse(Call<ApiCity> call, Response<ApiCity> response) {
                ApiCity resp = response.body();

                if (resp.getError() == "false") {
                    List<City> cityList = resp.getCityList();
                    List<String> stringList = new ArrayList<String>();

                    for (int i = 0; i < cityList.size(); i++) {
                        stringList.add(cityList.get(i).getCity_name());
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                            R.layout.support_simple_spinner_dropdown_item, stringList);
                    adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                    spinnerWilayah.setAdapter(adapter);
                } else {
                    new SweetAlertDialog(getApplicationContext(), SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Notif")
                            .setContentText(resp.getMessage())
                            .show();
                }
            }

            @Override
            public void onFailure(Call<ApiCity> call, Throwable t) {
                try {
                    throw new InterruptedException("Gagal Connect Server");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onValidationSucceeded() {

        swal = new SweetAlertDialog(CustomerActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        swal.setTitleText("Loading");
        swal.setContentText("Please wait...");
        swal.setCancelable(false);
        swal.show();

        RequestBody customer_name = RequestBody.create(MediaType.parse("text/plain"), etNama.getText().toString());
        RequestBody busines_id = RequestBody.create(MediaType.parse("text/plain"), selectedUsaha);
        RequestBody customer_group_id = RequestBody.create(MediaType.parse("text/plain"), selectedName);
        RequestBody customer_address = RequestBody.create(MediaType.parse("text/plain"), etAlamat.getText().toString());
        RequestBody customer_telp = RequestBody.create(MediaType.parse("text/plain"), etTelp.getText().toString());
        RequestBody customer_hp = RequestBody.create(MediaType.parse("text/plain"), etHp.getText().toString());
        RequestBody customer_npwp = RequestBody.create(MediaType.parse("text/plain"), etNoNPWP.getText().toString());
        RequestBody customer_npwp_name = RequestBody.create(MediaType.parse("text/plain"), etNamaNPWP.getText().toString());
        RequestBody customer_npwp_address = RequestBody.create(MediaType.parse("text/plain"), etAlamatNPWP.getText().toString());
        RequestBody customer_mail = RequestBody.create(MediaType.parse("text/plain"), etEmail.getText().toString());
        RequestBody city_id = RequestBody.create(MediaType.parse("text/plain"), selectedWilayah);
        RequestBody image = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part customer_img = MultipartBody.Part.createFormData("customer_img", file.getName(), image);
        RequestBody category_price_id = RequestBody.create(MediaType.parse("text/plain"), selectedHarga);
        RequestBody customer_limit_kredit = RequestBody.create(MediaType.parse("text/plain"), etLimitKredit.getText().toString());
        RequestBody customer_limit_card = RequestBody.create(MediaType.parse("text/plain"), "0");
        RequestBody customer_limit_day = RequestBody.create(MediaType.parse("text/plain"), etLimitTempo.getText().toString());
        RequestBody customer_card_no = RequestBody.create(MediaType.parse("text/plain"), "0");
        RequestBody customer_pic_name = RequestBody.create(MediaType.parse("text/plain"), etNama.getText().toString());
        RequestBody customer_pic_telp = RequestBody.create(MediaType.parse("text/plain"), etTelp.getText().toString());
        RequestBody customer_pic_address = RequestBody.create(MediaType.parse("text/plain"), etAlamat.getText().toString());
        RequestBody customer_store = RequestBody.create(MediaType.parse("text/plain"), etNamaToko.getText().toString());

        doCreateCustomer(customer_name, busines_id, customer_group_id, customer_address, customer_telp, customer_hp, customer_npwp,
                customer_npwp_name, customer_npwp_address, customer_mail, city_id, customer_img, category_price_id, customer_limit_kredit,
                customer_limit_card, customer_limit_day, customer_card_no, customer_pic_name, customer_pic_telp, customer_pic_address, customer_store);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(CustomerActivity.this);

            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(CustomerActivity.this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void doCreateCustomer(RequestBody customer_name, RequestBody busines_id, RequestBody customer_group_id,
                                  RequestBody customer_address, RequestBody customer_telp, RequestBody customer_hp,
                                  RequestBody customer_npwp, RequestBody customer_npwp_name, RequestBody customer_npwp_address,
                                  RequestBody customer_mail, RequestBody city_id, MultipartBody.Part customer_img, RequestBody category_price_id,
                                  RequestBody customer_limit_kredit, RequestBody customer_limit_card, RequestBody customer_limit_day,
                                  RequestBody customer_card_no, RequestBody customer_pic_name, RequestBody customer_pic_telp,
                                  RequestBody customer_pic_address, RequestBody customer_store) {

        ApiClient.get(this).postCustomer(customer_name, busines_id, customer_group_id, customer_address,
                customer_telp, customer_hp, customer_npwp, customer_npwp_name, customer_npwp_address, customer_mail, city_id,
                customer_img, category_price_id, customer_limit_kredit, customer_limit_card, customer_limit_day, customer_card_no,
                customer_pic_name, customer_pic_telp, customer_pic_address, customer_store).enqueue(new Callback<ApiCustomer>() {
            @Override
            public void onResponse(Call<ApiCustomer> call, Response<ApiCustomer> response) {
                ApiCustomer resp = response.body();

                if (resp.getError() == "false") {
                    swal.dismissWithAnimation();
                    new SweetAlertDialog(CustomerActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText("Notification")
                            .setContentText(resp.getMessage())
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    startActivity(new Intent(CustomerActivity.this, MainActivity.class));
                                    finish();
                                }
                            })
                            .show();
                } else {
                    swal.dismissWithAnimation();
                    new SweetAlertDialog(CustomerActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Notification")
                            .setContentText(resp.getMessage())
                            .show();
                }
            }

            @Override
            public void onFailure(Call<ApiCustomer> call, Throwable t) {
                try {
                    swal.dismissWithAnimation();
                    new SweetAlertDialog(CustomerActivity.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Notification")
                            .setContentText(t.getMessage())
                            .show();
                    t.printStackTrace();
                    throw new InterruptedException("Gagal Connect Server");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

    }
}
