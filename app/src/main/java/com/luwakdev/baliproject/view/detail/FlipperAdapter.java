package com.luwakdev.baliproject.view.detail;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.luwakdev.baliproject.R;
import com.luwakdev.baliproject.model.Slider;
import com.luwakdev.baliproject.utils.AppConfig;

import java.util.List;

public class FlipperAdapter extends BaseAdapter {

    private Context context;
    private List<Slider> sliders;

    public FlipperAdapter(Context context, List<Slider> sliders) {
        this.context = context;
        this.sliders = sliders;
    }

    @Override
    public int getCount() {
        return sliders.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Slider slider = sliders.get(position);

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.slider_item, null);

        ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
        Glide.with(context).load(AppConfig.BALI_IMAGE+slider.getItem_galery_file()).into(imageView);

        return view;
    }
}
