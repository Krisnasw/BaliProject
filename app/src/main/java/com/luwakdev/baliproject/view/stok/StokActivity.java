package com.luwakdev.baliproject.view.stok;

import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.luwakdev.baliproject.R;
import com.luwakdev.baliproject.model.ApiBarang;
import com.luwakdev.baliproject.model.Barang;
import com.luwakdev.baliproject.service.ApiClient;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StokActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.search_view)
    MaterialSearchView materialSearchView;
    @BindView(R.id.rvStok)
    RecyclerView recyclerView;

    private SweetAlertDialog sweetAlertDialog;
    private String title = "";

    private static final String TAG = StokFragment.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stok);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Bali System");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        sweetAlertDialog = new SweetAlertDialog(StokActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        sweetAlertDialog.setContentText("Please wait...");
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.show();

        getListBarang();

        materialSearchView.setVoiceSearch(true);
        materialSearchView.setHint("Masukkan Nama Barang Yang Dicari");
        materialSearchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                title = query;
                if (title.isEmpty()) {
                    return false;
                } else {
                    sweetAlertDialog = new SweetAlertDialog(StokActivity.this, SweetAlertDialog.PROGRESS_TYPE);
                    sweetAlertDialog.setContentText("Please wait...");
                    sweetAlertDialog.show();
                    getSearchBarang(title);
                    return false;
                }
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                title = newText;
                if (title.isEmpty()) {
                    return false;
                } else {
                    sweetAlertDialog = new SweetAlertDialog(StokActivity.this, SweetAlertDialog.PROGRESS_TYPE);
                    sweetAlertDialog.setContentText("Please wait...");
                    sweetAlertDialog.show();
                    getSearchBarang(title);
                    return false;
                }
            }
        });

        materialSearchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {

            }

            @Override
            public void onSearchViewClosed() {

            }
        });


        swipeRefreshLayout.setColorSchemeResources(R.color.light_blue, R.color.green, R.color.red);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshContent();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MaterialSearchView.REQUEST_VOICE && resultCode == RESULT_OK) {
            ArrayList<String> matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            if (matches != null && matches.size() > 0) {
                String searchWrd = matches.get(0);
                if (!TextUtils.isEmpty(searchWrd)) {
                    materialSearchView.setQuery(searchWrd, false);
                }
            }

            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        materialSearchView.setMenuItem(item);
        return true;
    }

    public void onBackPressed() {
        if (materialSearchView.isSearchOpen()) {
            materialSearchView.closeSearch();
        } else {
            super.onBackPressed();
        }
    }

    private void refreshContent() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        ApiClient.get(this).getListBarang().enqueue(new Callback<ApiBarang>() {
            @Override
            public void onResponse(Call<ApiBarang> call, Response<ApiBarang> response) {
                if (response.body().getError() == "false") {
                    sweetAlertDialog.dismissWithAnimation();
                    List<Barang> barangs = response.body().getBarangs();
                    Log.e(TAG, "Jumlah Barang : "+barangs.size());
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(new StokAdapter(barangs, R.layout.stok_item, getApplicationContext()));
                    swipeRefreshLayout.setRefreshing(false);
                } else {
                    sweetAlertDialog.dismissWithAnimation();
                    new SweetAlertDialog(StokActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Notification")
                            .setContentText(response.body().getMessage())
                            .show();
                }
            }

            @Override
            public void onFailure(Call<ApiBarang> call, Throwable t) {
                try {
                    throw new InterruptedException("Gagal Connect Server");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void getListBarang() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        ApiClient.get(this).getListBarang().enqueue(new Callback<ApiBarang>() {
            @Override
            public void onResponse(Call<ApiBarang> call, Response<ApiBarang> response) {
                if (response.body().getError() == "false") {
                    sweetAlertDialog.dismissWithAnimation();
                    List<Barang> barangs = response.body().getBarangs();
                    Log.e(TAG, "Jumlah Barang : "+barangs.size());
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(new StokAdapter(barangs, R.layout.stok_item, getApplicationContext()));
                } else {
                    sweetAlertDialog.dismissWithAnimation();
                    new SweetAlertDialog(StokActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Notification")
                            .setContentText(response.body().getMessage())
                            .show();
                }
            }

            @Override
            public void onFailure(Call<ApiBarang> call, Throwable t) {
                try {
                    throw new InterruptedException("Gagal Connect Server");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void getSearchBarang(String keys) {
        ApiClient.get(this).getSearchBarang(keys).enqueue(new Callback<ApiBarang>() {
            @Override
            public void onResponse(Call<ApiBarang> call, Response<ApiBarang> response) {
                ApiBarang resp = response.body();
                if (resp.getError() == "false") {
                    sweetAlertDialog.dismissWithAnimation();
                    List<Barang> barangs = response.body().getBarangs();
                    Log.e(TAG, "Jumlah Barang : "+barangs.size());
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(new StokAdapter(barangs, R.layout.stok_item, getApplicationContext()));
                } else {
                    sweetAlertDialog.dismissWithAnimation();
                    new SweetAlertDialog(StokActivity.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Notification")
                            .setContentText(resp.getMessage())
                            .show();
                }
            }

            @Override
            public void onFailure(Call<ApiBarang> call, Throwable t) {
                try {
                    sweetAlertDialog.dismissWithAnimation();
                    new SweetAlertDialog(StokActivity.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Notification")
                            .setContentText("Gagal Connect Ke Server")
                            .show();
                    throw new InterruptedException("Gagal Connect Server");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

}
