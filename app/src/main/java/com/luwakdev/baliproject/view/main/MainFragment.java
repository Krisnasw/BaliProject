package com.luwakdev.baliproject.view.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.luwakdev.baliproject.R;
import com.luwakdev.baliproject.utils.AppConfig;
import com.luwakdev.baliproject.utils.CustomGridViewActivity;
import com.luwakdev.baliproject.view.about.AboutActivity;
import com.luwakdev.baliproject.view.about.AboutFragment;
import com.luwakdev.baliproject.view.customer.CustomerActivity;
import com.luwakdev.baliproject.view.customer.CustomerFragment;
import com.luwakdev.baliproject.view.konfirmasi.ConfirmActivity;
import com.luwakdev.baliproject.view.konfirmasi.ConfirmFragment;
import com.luwakdev.baliproject.view.login.LoginActivity;
import com.luwakdev.baliproject.view.stok.StokActivity;
import com.luwakdev.baliproject.view.stok.StokFragment;
import com.pixplicity.easyprefs.library.Prefs;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainFragment extends Fragment {

    @BindView(R.id.grid_view_image_text)
    GridView gridView;

    String[] menuName = {
            "Pengelolaan Stok", "Konfirmasi DO", "Tambah Customer", "Tentang Kami"
    };

    int[] menuImage = {
            R.drawable.ic_pengelolaan, R.drawable.ic_konfirm, R.drawable.ic_customer, R.drawable.ic_about
    };

    public MainFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_home, container, false);

        ButterKnife.bind(this, view);

        CustomGridViewActivity customGridViewActivity = new CustomGridViewActivity(getContext(), menuName, menuImage);
        gridView.setAdapter(customGridViewActivity);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        startActivity(new Intent(getActivity(), StokActivity.class));
                        break;
                    case 1:
                        startActivity(new Intent(getActivity(), ConfirmActivity.class));
                        break;
                    case 2:
                        startActivity(new Intent(getActivity(), CustomerActivity.class));
                        break;
                    case 3:
                        startActivity(new Intent(getActivity(), AboutActivity.class));
                        break;
                }
            }
        });

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
