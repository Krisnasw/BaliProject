package com.luwakdev.baliproject.view.pelunasan;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.luwakdev.baliproject.R;
import com.luwakdev.baliproject.model.ApiPelunasan;
import com.luwakdev.baliproject.model.ApiWarehouse;
import com.luwakdev.baliproject.model.Nota;
import com.luwakdev.baliproject.model.Warehouse;
import com.luwakdev.baliproject.service.ApiClient;
import com.luwakdev.baliproject.utils.AppConfig;
import com.luwakdev.baliproject.view.stok.StokActivity;
import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.pixplicity.easyprefs.library.Prefs;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class PelunasanFragment extends Fragment {

    @BindView(R.id.spinnerCabang)
    Spinner spinner;
    @BindView(R.id.rvList)
    RecyclerView recyclerView;
    @BindView(R.id.progress)
    ProgressBar progressBar;
    @BindView(R.id.tvTotal)
    TextView total;
    @BindView(R.id.btnFilterNota)
    Button btnFilterNota;
    @BindView(R.id.btnTanggal)
    Button btnFilterTanggal;
    @BindView(R.id.btnSubmitFilter)
    Button btnFilter;

    private String selectedCabang = "";
    private SweetAlertDialog sweetAlertDialog;
    private String title = "";
    private int month;
    private StringBuilder mcurrentDate;

    private String date, nota_code;

    public PelunasanFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_pelunasan, container, false);

        ButterKnife.bind(this, view);

        recyclerView.setVisibility(View.GONE);
        sweetAlertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);

        initSpinnerCabang();

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedCabang = parent.getItemAtPosition(position).toString();
                recyclerView.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                callNotaByCabang(selectedCabang);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnFilterNota.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFilterKodeNota();
            }
        });

        btnFilterTanggal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker();
            }
        });

        btnFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initFilterable();
            }
        });

        return view;
    }

    private void initSpinnerCabang() {
        ApiClient.get(getActivity()).getListWarehouse().enqueue(new Callback<ApiWarehouse>() {
            @Override
            public void onResponse(Call<ApiWarehouse> call, Response<ApiWarehouse> response) {
                ApiWarehouse resp = response.body();

                if (resp.getError() == "false") {
                    List<Warehouse> warehouseList = resp.getWarehouseList();
                    List<String> stringList = new ArrayList<String>();

                    for (int i = 0; i < warehouseList.size(); i++) {
                        stringList.add(warehouseList.get(i).getWarehouse_name());
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                            android.R.layout.simple_spinner_item, stringList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner.setAdapter(adapter);
                } else {
                    new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Notification")
                            .setContentText(resp.getMessage())
                            .show();
                }
            }

            @Override
            public void onFailure(Call<ApiWarehouse> call, Throwable t) {
                try {
                    new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Notification")
                            .setContentText("Terjadi Kesalahan")
                            .show();
                    throw new InterruptedException("Terjadi Kesalah");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void callNotaByCabang(String cabangName) {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        ApiClient.get(getActivity()).cekPelunasan(cabangName).enqueue(new Callback<ApiPelunasan>() {
            @Override
            public void onResponse(Call<ApiPelunasan> call, Response<ApiPelunasan> response) {
                ApiPelunasan resp = response.body();

                if (resp.getError() == "false") {
                    progressBar.setVisibility(View.GONE);
                    List<Nota> notas = resp.getNotaList();
                    total.setText("Total Bayar : "+new DecimalFormat("#,###,###").format(Integer.parseInt(resp.getTotal())));
                    recyclerView.setVisibility(View.VISIBLE);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(new PelunasanAdapter(notas, R.layout.pelunasan_item, getActivity()));
                } else {
                    new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Notification")
                            .setContentText(resp.getMessage())
                            .show();
                }
            }

            @Override
            public void onFailure(Call<ApiPelunasan> call, Throwable t) {
                try {
                    new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Notification")
                            .setContentText("Terjadi Kesalahan")
                            .show();
                    throw new InterruptedException("Terjadi Kesalah");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void showFilterKodeNota() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_item, null);
        dialogBuilder.setView(dialogView);

        final MaterialEditText editText = (MaterialEditText) dialogView.findViewById(R.id.etFilterNota);
        dialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                btnFilterNota.setText("Nota : "+editText.getText().toString());
                nota_code = editText.getText().toString();
                Toast.makeText(getActivity(), "Kode Nota : "+editText.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                btnFilterNota.setText("Kode Nota");
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    private void showDatePicker() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.datepicker_item, null);
        dialogBuilder.setView(dialogView);

        final DatePicker datePicker = (DatePicker) dialogView.findViewById(R.id.datePicker);

        dialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mcurrentDate = new StringBuilder();
                month = datePicker.getMonth() + 1;
                mcurrentDate.append(datePicker.getDayOfMonth() + "-" + month + "-" + datePicker.getYear());
                btnFilterTanggal.setText("Tanggal : "+mcurrentDate.toString());
                date = mcurrentDate.toString();
                Toast.makeText(getActivity(), "Tanggal : "+mcurrentDate.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                btnFilterTanggal.setText("Tanggal");
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    private void initFilterable() {
        ApiClient.get(getActivity()).filterPelunasan(selectedCabang, date, nota_code).enqueue(new Callback<ApiPelunasan>() {
            @Override
            public void onResponse(Call<ApiPelunasan> call, Response<ApiPelunasan> response) {
                ApiPelunasan resp = response.body();

                if (resp.getError() == "false") {
                    progressBar.setVisibility(View.GONE);
                    List<Nota> notas = resp.getNotaList();
                    total.setText("Total Bayar : "+new DecimalFormat("#,###,###").format(Integer.parseInt(resp.getTotal())));
                    recyclerView.setVisibility(View.VISIBLE);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(new PelunasanAdapter(notas, R.layout.pelunasan_item, getActivity()));
                } else {
                    new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Notification")
                            .setContentText(resp.getMessage())
                            .show();
                }
            }

            @Override
            public void onFailure(Call<ApiPelunasan> call, Throwable t) {
                try {
                    new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Notification")
                            .setContentText(t.getMessage().toString())
                            .show();
                    throw new InterruptedException("Terjadi Kesalah");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
