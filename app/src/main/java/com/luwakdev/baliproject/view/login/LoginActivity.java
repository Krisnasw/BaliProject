package com.luwakdev.baliproject.view.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.luwakdev.baliproject.R;
import com.luwakdev.baliproject.model.ApiLogin;
import com.luwakdev.baliproject.service.ApiClient;
import com.luwakdev.baliproject.utils.AppConfig;
import com.luwakdev.baliproject.utils.MyEditText;
import com.luwakdev.baliproject.utils.MyTextView;
import com.luwakdev.baliproject.view.main.MainActivity;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements Validator.ValidationListener {

    @BindView(R.id.etUsername)
            @NotEmpty
    MyEditText etUsername;
    @BindView(R.id.etPassword)
            @NotEmpty
    MyEditText etPassword;
    @BindView(R.id.btnSubmit)
    MyTextView btnSubmit;

    Validator validator;
    String userData;
    SweetAlertDialog sweetAlertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);

        if (Prefs.getBoolean(AppConfig.KEY_ISLOGGEDIN, false)) {
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();
        }

        validator = new Validator(this);
        validator.setValidationListener(this);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validator.validate();
            }
        });
    }

    @Override
    public void onValidationSucceeded() {
        sweetAlertDialog = new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        sweetAlertDialog.setContentText("Please wait...");
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.show();
        doLogin(etUsername.getText().toString().trim(), etPassword.getText().toString().trim());
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void doLogin(String username, String password) {
        ApiClient.get(this).login(username, password).enqueue(new Callback<ApiLogin>() {
            @Override
            public void onResponse(Call<ApiLogin> call, Response<ApiLogin> response) {
                final ApiLogin resp = response.body();
                if (resp.getError() != "true") {
                    sweetAlertDialog.dismissWithAnimation();
                    new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText("Success")
                            .setContentText(resp.getMessage())
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    userData = resp.getData();
                                    navigateToMain();
                                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                                    finish();
                                }
                            })
                            .show();
                } else {
                    sweetAlertDialog.dismissWithAnimation();
                    new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Error")
                            .setContentText(resp.getMessage())
                            .show();
                }
            }

            @Override
            public void onFailure(Call<ApiLogin> call, Throwable t) {
                try {
                    throw new InterruptedException("Gagal Connect Server");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void navigateToMain() {
        Prefs.putString(AppConfig.KEY_USERNAME, userData);
        Prefs.putBoolean(AppConfig.KEY_ISLOGGEDIN, true);
    }
}