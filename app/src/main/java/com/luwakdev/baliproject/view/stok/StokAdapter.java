package com.luwakdev.baliproject.view.stok;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.luwakdev.baliproject.R;
import com.luwakdev.baliproject.model.Barang;
import com.luwakdev.baliproject.view.detail.DetailActivity;

import java.text.DecimalFormat;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StokAdapter extends RecyclerView.Adapter<StokAdapter.MyViewHolder> {

    private List<Barang> barangs;
    private int rowLayout;
    private Context context;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.layoutStok)
        LinearLayout linearLayout;
        @BindView(R.id.tvNamaBarang)
        TextView namaBarang;
        @BindView(R.id.tvBarcode)
        TextView barcode;
        @BindView(R.id.tvHarga)
        TextView harga;
        @BindView(R.id.tvQty)
        TextView qty;
        @BindView(R.id.btnImageView)
        Button btnImage;

        public MyViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

    }

    public StokAdapter(List<Barang> barangs, int rowLayout, Context context) {
        this.barangs = barangs;
        this.rowLayout = rowLayout;
        this.context = context;
    }

    @Override
    public StokAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.namaBarang.setText("Nama Barang : "+barangs.get(position).getItem_name());
        holder.barcode.setText("Barcode : "+barangs.get(position).getItem_barcode());
        holder.harga.setText("Harga : "+new DecimalFormat("#,###,###").format(barangs.get(position).getItem_price1()));
        holder.qty.setText("Stok : "+barangs.get(position).getItem_per_unit());
        holder.btnImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, DetailActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt("id", barangs.get(position).getItem_id());
                i.putExtras(bundle);
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return barangs.size();
    }

}
