package com.luwakdev.baliproject;

import android.app.Application;
import android.content.ContextWrapper;
import android.os.StrictMode;
import android.widget.Toast;

import com.luwakdev.baliproject.utils.AppStatus;
import com.pixplicity.easyprefs.library.Prefs;

public class BaliApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        new Prefs.Builder()
                .setContext(this)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName(getPackageName())
                .setUseDefaultSharedPreference(true)
                .build();

        if (AppStatus.getInstance(this).isOnline()) {
        } else {
            Toast.makeText(this, "Tidak Ada Koneksi Internet", Toast.LENGTH_SHORT).show();
        }
    }

}
