package com.luwakdev.baliproject.service;

import com.luwakdev.baliproject.model.ApiBarang;
import com.luwakdev.baliproject.model.ApiBusiness;
import com.luwakdev.baliproject.model.ApiCart;
import com.luwakdev.baliproject.model.ApiCategoryPrice;
import com.luwakdev.baliproject.model.ApiCity;
import com.luwakdev.baliproject.model.ApiConfirmed;
import com.luwakdev.baliproject.model.ApiCustomer;
import com.luwakdev.baliproject.model.ApiCustomerGroup;
import com.luwakdev.baliproject.model.ApiKonfirmasi;
import com.luwakdev.baliproject.model.ApiListCustomer;
import com.luwakdev.baliproject.model.ApiLogin;
import com.luwakdev.baliproject.model.ApiPelunasan;
import com.luwakdev.baliproject.model.ApiPiutang;
import com.luwakdev.baliproject.model.ApiSlider;
import com.luwakdev.baliproject.model.ApiUser;
import com.luwakdev.baliproject.model.ApiWarehouse;
import com.luwakdev.baliproject.model.User;

import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface ApiInterface {

    @FormUrlEncoded
    @POST("auth/login")
    Call<ApiLogin> login(@Field("username") String username, @Field("password") String password);

    @GET("auth/user/{username}")
    Call<ApiUser> getUserData(@Path("username") String username);

    @GET("barang/list")
    Call<ApiBarang> getListBarang();

    @GET("barang/search/{keys}")
    Call<ApiBarang> getSearchBarang(@Path("keys") String keys);

    @Multipart
    @POST("customer/add")
    Call<ApiCustomer> postCustomer(@Part("customer_name") RequestBody customer_name, @Part("busines_id") RequestBody busines_id,
                                   @Part("customer_group_id") RequestBody customer_group_id, @Part("customer_address") RequestBody customer_address,
                                   @Part("customer_telp") RequestBody customer_telp, @Part("customer_hp") RequestBody customer_hp,
                                   @Part("customer_npwp") RequestBody customer_npwp, @Part("customer_npwp_name") RequestBody customer_npwp_name,
                                   @Part("customer_npwp_address") RequestBody customer_npwp_address, @Part("customer_mail") RequestBody customer_mail,
                                   @Part("city_id") RequestBody city_id, @Part MultipartBody.Part customer_img,
                                   @Part("category_price_id") RequestBody category_price_id, @Part("customer_limit_kredit") RequestBody customer_limit_kredit,
                                   @Part("customer_limit_card") RequestBody customer_limit_card, @Part("customer_limit_day") RequestBody customer_limit_day,
                                   @Part("customer_card_no") RequestBody customer_card_no, @Part("customer_pic_name") RequestBody customer_pic_name,
                                   @Part("customer_pic_telp") RequestBody customer_pic_telp, @Part("customer_pic_address") RequestBody customer_pic_address,
                                   @Part("customer_store") RequestBody customer_store);

    @GET("customerGroup/list")
    Call<ApiCustomerGroup> getListCustomerGroup();

    @GET("categoryPrice/list")
    Call<ApiCategoryPrice> getListCategoryPrice();

    @GET("business/list")
    Call<ApiBusiness> getListBusiness();

    @GET("city/list")
    Call<ApiCity> getListCity();

    @GET("warehouse/list")
    Call<ApiWarehouse> getListWarehouse();

    @FormUrlEncoded
    @POST("cek/pelunasan")
    Call<ApiPelunasan> cekPelunasan(@Field("warehouse_name") String warehouse_name);

    @GET("customer/list")
    Call<ApiListCustomer> getListCustomer();

    @FormUrlEncoded
    @POST("cek/piutang")
    Call<ApiPiutang> cekPiutang(@Field("customer_name") String customer_name);

    @GET("filter/piutang/{kode_nota}/{date}/{customer_name}")
    Call<ApiPiutang> filterPiutang(@Path("customer_name") String customer_name, @Path("date") String date, @Path("kode_nota") String kode_nota);

    @GET("filter/pelunasan/{kode_nota}/{date}/{warehouse_name}")
    Call<ApiPelunasan> filterPelunasan(@Path("warehouse_name") String warehouse_name, @Path("date") String date, @Path("kode_nota") String kode_nota);

    @GET("barang/image/{id}")
    Call<ApiSlider> getSliderList(@Path("id") Integer id);

    @GET("delivery/{delivery_id}")
    Call<ApiKonfirmasi> getKonfirmasiData(@Path("delivery_id") Integer delivery_id);

    @FormUrlEncoded
    @POST("cart/update")
    Call<ApiCart> postUpdateCart(@Field("order_id") Integer delivery_id, @Field("quantity") String quantity);

    @FormUrlEncoded
    @POST("cart/confirmed")
    Call<ApiConfirmed> postConfirmedCart(@Field("delivery_id") Integer delivery_id);
}