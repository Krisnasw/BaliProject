package com.luwakdev.baliproject.utils;

public class AppConfig {

    public static final String IMAGE_URL = "https://mbolang.luwakdev.com/images/";
    public static final String API_URL = "https://mbolang.luwakdev.com/api/v1/";
    public static final String BALI_IMAGE = "http://jasasoftdroid.com/bali/images/item/";

    // KEY
    public static final String KEY_USERNAME = "user_username";
    public static final String KEY_ISLOGGEDIN = "is_logged_in";
    public static final String KEY_CONFIRM = "is_confirm";

}
